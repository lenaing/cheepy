module main

import vweb

pub fn (mut app App) index() vweb.Result {
	title := 'SMK Leaderboard Tracker'

	mut countries_map := map[string]string{}
	for country in app.service_get_all_countries() or { [] } {
		countries_map[country.id] = country.name
	}

	players := app.service_get_all_players() or { [] }
	app_version := app.version
	return $vweb.html()
}
