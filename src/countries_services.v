module main

import database { Country }

fn (mut app App) service_get_all_countries() ![]Country {
	mut db := database.create_db_connection(app.config.db) or {
		println(err)
		return err
	}

	countries := sql db {
		select from Country order by id
	}!

	return countries
}
