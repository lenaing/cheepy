module database

import time

[table: 'update_cache']
pub struct UpdateCache {
	link_name string    [primary]
	post_date time.Time [sql_type: 'TIMESTAMP']
}
