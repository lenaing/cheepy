module database

import db.sqlite
import orm
import config

pub fn create_db_connection(conf config.DBConfig) !orm.Connection {
	return orm.Connection(sqlite.connect(conf.database)!)
}
