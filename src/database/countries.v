module database

[table: 'countries']
pub struct Country {
pub:
	id   string [primary]
	name string
}
