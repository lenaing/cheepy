module database

[table: 'players']
pub struct Player {
pub:
	id       int    [primary]
	name     string
	country  string
	location string
}
