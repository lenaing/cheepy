module database

import time

pub enum GameType {
	pal
	ntsc
}

[table: 'rankings']
pub struct Ranking {
pub:
	ranking_date time.Time [sql_type: 'TIMESTAMP'; unique: 'ranking']
	game_type    GameType  [unique: 'ranking']
	player       string    [unique: 'ranking']
	rank         int       [unique: 'ranking']
	new_member   bool
}
