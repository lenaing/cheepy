module main

import cli
import net
import os
import vweb
import v.vmod
import config
import updater

struct App {
	vweb.Context
	config  config.Config [vweb_global]
	version string        [vweb_global]
}

pub fn (app App) before_request() {
	println('${app.req.method} ${app.req.url}')
}

fn main() {
	vm := vmod.decode(@VMOD_FILE) or { panic(err) }
	mut app := cli.Command{
		name: '${vm.name}'
		description: '${vm.description}'
		version: '${vm.version}'
		execute: run
		flags: [
			cli.Flag{
				flag: .string
				name: 'listen'
				abbrev: 'l'
				description: 'Listens for connections on given IP.'
				default_value: ['']
			},
			cli.Flag{
				flag: .int
				name: 'port'
				abbrev: 'p'
				description: 'Listens for connections on given port.'
				default_value: ['-1']
			},
			cli.Flag{
				flag: .string
				name: 'database'
				abbrev: 'D'
				description: 'Connect to given backend database URI.'
				default_value: ['']
			},
			cli.Flag{
				flag: .string
				name: 'config'
				abbrev: 'c'
				description: 'Load configuration from given TOML file.'
			},
			cli.Flag{
				flag: .bool
				name: 'update'
				abbrev: 'u'
				description: 'Update database from SMK site news.'
				default_value: ['false']
			},
			cli.Flag{
				flag: .bool
				name: 'force-update'
				abbrev: 'f'
				description: 'Force database refresh by dropping all data first.'
				default_value: ['false']
			},
		]
	}
	app.setup()
	app.parse(os.args)
}

fn run(cmd cli.Command) ! {
	conf := config.parse_config(cmd)!

	should_update := cmd.flags.get_bool('update')!
	if should_update {
		force_update := cmd.flags.get_bool('force-update')!
		updater.update_db(conf, force_update)!
		return
	}

	vm := vmod.decode(@VMOD_FILE) or { panic(err) }
	println('${vm.name} v${vm.version} starting on ${conf.ip}:${conf.port} ...')

	mut app := &App{
		config: conf
		version: cmd.version
	}
	app.mount_static_folder_at(os.resource_abs_path('.'), '/')
	vweb.run_at(app, vweb.RunParams{
		family: net.AddrFamily.ip
		host: conf.ip
		port: conf.port
	}) or { panic(err) }
}
