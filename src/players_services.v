module main

import database { Player }

fn (mut app App) service_get_all_players() ![]Player {
	mut db := database.create_db_connection(app.config.db) or {
		println(err)
		return err
	}

	players := sql db {
		select from Player order by name
	}!

	return players
}

fn (mut app App) service_get_player_by_id(id int) !Player {
	mut db := database.create_db_connection(app.config.db) or {
		println(err)
		return err
	}

	mut results := []Player{}
	results << sql db {
		select from Player where id == id
	}!
	if results.len == 0 {
		return error("No such player: '${id}'")
	}
	return results[0]
}

fn (mut app App) service_get_player_by_name(name string) !Player {
	mut db := database.create_db_connection(app.config.db) or {
		println(err)
		return err
	}

	mut results := []Player{}
	results << sql db {
		select from Player where name == name
	}!
	if results.len == 0 {
		return error("No such player: '${name}'")
	}

	return results[0]
}
