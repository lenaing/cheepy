module updater

import regex
import database { GameType }

const (
	welcome_strings = ['No new member', 'Welcome to...', 'Please welcome to...']
)

fn parse_header_regex(src string) string {
	query := r'(?P<day>[0-9]+) (?P<month>[a-zA-Z]+) (?P<year>[0-9]+):'
	mut re := regex.regex_opt(query) or { panic(err) }
	start, _ := re.match_string(src)
	if start < 0 {
		panic("Failed to parse header: '${src}'")
	}

	day_start, day_end := re.get_group_bounds_by_name('day')
	day := src[day_start..day_end].int()
	month_start, month_end := re.get_group_bounds_by_name('month')
	month := match src[month_start..month_end] {
		'January' {
			'01'
		}
		'February' {
			'02'
		}
		'March' {
			'03'
		}
		'April' {
			'04'
		}
		'May' {
			'05'
		}
		'June' {
			'06'
		}
		'July' {
			'07'
		}
		'August' {
			'08'
		}
		'September' {
			'09'
		}
		'October' {
			'10'
		}
		'November' {
			'11'
		}
		'December' {
			'12'
		}
		else {
			panic('Unknown month in header: ${src}')
		}
	}
	year_start, year_end := re.get_group_bounds_by_name('year')
	year := src[year_start..year_end].int()
	return '${year}-${month}-${day} 00:00:00'
}

fn parse_new_member_regex(src string) ?(string, int, GameType) {
	if src.count('<font color="00ff00" >') == 0 {
		$if debug {
			mut found := false
			for welcome_string in updater.welcome_strings {
				if src.to_lower().starts_with(welcome_string.to_lower()) {
					found = true
				}
			}
			if !found {
				println("⚠️ Not a new member line: '${src}'.")
			}
		}
		return none
	}

	query := r'<font color="00ff00" >(?P<player>.*)</font>.*\(([Jj]oining )?(?P<gameType>[A-Z]+),?( Worldwide)? (?P<rank>[0-9]+).*,'
	mut re := regex.regex_opt(query) or { panic(err) }
	start, _ := re.match_string(src)
	if start < 0 {
		panic("Failed to parse new member ranking in '${src}'.")
	}

	rank_start, rank_end := re.get_group_bounds_by_name('rank')
	rank := src[rank_start..rank_end].int()
	player_start, player_end := re.get_group_bounds_by_name('player')
	player := src[player_start..player_end]
	game_type_start, game_type_end := re.get_group_bounds_by_name('gameType')
	game_type_str := src[game_type_start..game_type_end]
	game_type := match game_type_str {
		'NTSC' {
			GameType.ntsc
		}
		'PAL' {
			GameType.pal
		}
		else {
			panic('Unknown game type: ${game_type_str}')
		}
	}

	return player, rank, game_type
}

fn parse_rankings_regex(src string) ?map[string]int {
	champion := r'((The)|(We (now )?have a new)|(Our newly crowned)|(A new)|(New) )?(PAL)|(NTSC) (Version )?((Tii?me Trial)|(TT)) Worr?ld (Champion)|(Title).*<font color="00ffff" >(?P<player>.*)</font>'
	champion_re := regex.regex_opt(champion) or { panic(err) }
	classic := r'.*#(?P<rank>[0-9]+).*<font color="00ffff" >(?P<player>.*)</font>'
	classic_re := regex.regex_opt(classic) or { panic(err) }
	weird := r'.*<font color="00ffff" >(?P<player>.*)</font>.*#(?P<rank>[0-9]+).*'
	weird_re := regex.regex_opt(weird) or { panic(err) }
	font_end_len := '</font>'.len

	mut input := src
	sharps := input.count('#')
	colors := input.count('<font color="00ffff" >')

	mut rankings := map[string]int{}
	for {
		// Champion rank
		champion_start, _ := champion_re.match_string(input)
		if champion_start >= 0 {
			player_start, player_end := champion_re.get_group_bounds_by_name('player')
			player := input[player_start..player_end]
			rankings[player] = 1
			input = input[player_end + font_end_len..]
		}

		// Classic rank
		classic_start, _ := classic_re.match_string(input)
		if classic_start >= 0 {
			rank_start, rank_end := classic_re.get_group_bounds_by_name('rank')
			rank := input[rank_start..rank_end].int()
			player_start, player_end := classic_re.get_group_bounds_by_name('player')
			player := input[player_start..player_end]
			rankings[player] = rank
			input = input[player_end + font_end_len..]
		}

		if champion_start < 0 && classic_start < 0 {
			// Weird rank
			weird_start, _ := weird_re.match_string(input)
			if weird_start >= 0 {
				player_start, player_end := weird_re.get_group_bounds_by_name('player')
				player := input[player_start..player_end]
				rank_start, rank_end := weird_re.get_group_bounds_by_name('rank')
				rank := input[rank_start..rank_end].int()
				rankings[player] = rank
				input = input[rank_end..]
			} else {
				break
			}
		}
	}

	found_len := rankings.len
	if found_len != colors && found_len != sharps {
		// If we don't find the same number of rankings as the font tag count
		// and the rankings count is not the same as the sharps count in the string
		// something went wrong
		$if debug {
			println("⚠️ Wrong count of rankings, found/sharps/colors : ${found_len}/${sharps}/${colors}, src: '${src}'.")
		}
		return none
	}

	return rankings
}
