module updater

import net.html
import net.http
import regex
import database { Country, Player }

const (
	profiles_url = 'https://www.mariokart64.com/smk/profile.php'
)

fn parse_name_regex(src string) (int, string) {
	query := '.*pid=(?P<id>[0-9]+)" >(?P<name>.*)<'
	mut re := regex.regex_opt(query) or { panic(err) }
	start, _ := re.match_string(src)
	if start < 0 {
		panic("Failed to parse player name: '${src}'")
	}

	id_start, id_end := re.get_group_bounds_by_name('id')
	id := src[id_start..id_end]
	name_start, name_end := re.get_group_bounds_by_name('name')
	name := src[name_start..name_end]
	return id.int(), name
}

fn parse_country_regex(src string) (string, string) {
	query := r'.*(?P<id>[A-Z]+).png".*title="(?P<title>[a-zA-Z ]+)".*'
	mut re := regex.regex_opt(query) or { panic(err) }
	start, _ := re.match_string(src)
	if start < 0 {
		panic("Failed to parse country: '${src}'")
	}

	id_start, id_end := re.get_group_bounds_by_name('id')
	id := src[id_start..id_end]
	title_start, title_end := re.get_group_bounds_by_name('title')
	title := src[title_start..title_end]
	return id, title
}

fn get_players_and_countries() ([]Player, []Country) {
	resp := http.get(updater.profiles_url) or { panic(err) }
	mut doc := html.parse(resp.body)
	players_table := doc.get_tags_by_attribute_value('class', 'playerslist')[0]
	mut players := []Player{}
	mut countries_map := map[string]string{}
	for row in players_table.get_tags('tr') {
		columns := row.get_tags('td')
		if columns.len == 3 {
			name_column := columns[0]
			player_id, player_name := parse_name_regex(name_column.str())

			country_column := columns[1]
			country_id, country_title := parse_country_regex(country_column.str())
			countries_map[country_id] = country_title

			location_column := columns[2]
			player_location := location_column.text()

			players << Player{player_id, player_name, country_id, player_location}
		}
	}
	mut countries := []Country{}
	for id, title in countries_map {
		countries << Country{id, title}
	}
	return players, countries
}
