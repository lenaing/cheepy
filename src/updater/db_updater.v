module updater

import config { Alias, Config }
import database { Country, Player, Ranking, UpdateCache }

pub fn update_db(conf Config, force_update bool) ! {
	mut db := database.create_db_connection(conf.db) or {
		println(err)
		return err
	}

	// Force database refresh
	if force_update {
		sql db {
			drop table Country
			drop table Player
			drop table Ranking
			drop table UpdateCache
		} or {}
	}

	// Create database structure
	sql db {
		create table Country
		create table Player
		create table Ranking
		create table UpdateCache
	}!

	println('🐟 Cheepy Updater')

	// Get players and countries
	mut players, mut countries := get_players_and_countries()
	for mut player in players {
		sql db {
			insert player into Player
		} or {}
	}
	for mut country in countries {
		sql db {
			insert country into Country
		} or {}
	}

	// Update progress
	links := get_archives_links()
	mut cnt := 0
	total := links.len
	mut progress := ''
	for link in links[cnt..] {
		flush_stdout()
		mut cached := false
		link_cached := sql db {
			select from UpdateCache where link_name == link limit 1
		}!
		if link_cached.len != 0 {
			cached = true
		}

		cnt += 1

		$if !debug {
			// Funny prompt
			if cnt % 20 == 0 {
				progress = ''
				for _ in 0 .. 80 {
					print(' ')
				}
				print('\r')
			} else {
				progress += if cached {
					'✨'
				} else {
					'💨'
				}
			}
			print('${cnt:04}/${total:04} 🚙${progress}\r')
		} $else {
			println('${cnt:04}/${total:04} ${link}')
		}

		// Link already scrapped: skip it
		if cached {
			continue
		}

		// Hit Leaderboard's site
		ranking_date, mut rankings := get_archive_post(link)
		for mut ranking in rankings {
			// Check for player we don't want to track
			if ranking.player in conf.do_not_track {
				$if debug {
					println('⚠️ Found player that we do not want to track: ${ranking.player}')
				}
				continue
			}

			// Check for aliases and update accordingly
			if alias := search_for_alias(ranking.player, conf.aliases) {
				$if debug {
					println("🔰 Found alias of '${alias}': ${ranking.player}")
				}
				ranking = Ranking{
					...ranking
					player: alias
				}
			}

			sql db {
				insert ranking into Ranking
			}!
		}

		// Update scrapped links cache
		cache := UpdateCache{link, ranking_date}
		sql db {
			insert cache into UpdateCache
		}!
	}
	println('')
}

fn search_for_alias(playername string, aliases []Alias) ?string {
	alias := aliases.filter(it.aliases.contains(playername))
	if alias.len > 0 {
		return alias[0].player
	}
	return none
}
