module updater

import net.html
import net.http
import time
import database { GameType, Ranking }

const (
	archives_url = 'https://www.mariokart64.com/smk/archives/'
)

fn get_archives_links() []string {
	resp := http.get(updater.archives_url) or { panic(err) }
	mut doc := html.parse(resp.body)
	archive_table := doc.get_tags_by_attribute_value('id', 'mycontent')[0]
		.get_tags('table')[1]
	mut links := []string{}
	for link in archive_table.get_tags('a') {
		links << link.attributes['href']
	}
	return links
}

fn get_archive_post(url string) (time.Time, []Ranking) {
	resp := http.get(url) or { panic(err) }
	mut doc := html.parse(resp.body)
	post := doc.get_tags_by_attribute_value('id', 'mycontent')[0]
		.get_tags('table')[0]
	post_header := post.get_tags('th')[0].text()
	post_date := time.parse(parse_header_regex(post_header)) or { panic(err) }
	post_body := post.get_tags('td')[0].str()
	return post_date, parse_post(post_date, post_body)
}

enum ParsingState {
	unknown
	new_members
	new_world_records
	pal
	ntsc
}

fn remove_tags(src string, tags []string) string {
	mut res := src
	for tag in tags {
		res = res.replace('<${tag}>', '').replace('</${tag}>', '')
	}
	return res
}

fn parse_post(date time.Time, src string) []Ranking {
	content_lines := remove_tags(src, ['b', 'td', 'text'])
		.replace('<br/>', '\n')
		.split_into_lines().filter(!it.is_blank())
	mut state := ParsingState.unknown

	mut rankings := []Ranking{}
	for line in content_lines {
		// Switch parsing mode
		match line {
			'<font color="#ffcc33" >New Members</font>' {
				state = ParsingState.new_members
				continue
			}
			'<font color="#ffcc33" >New World Records</font>' {
				state = ParsingState.new_world_records
				continue
			}
			'<font color="#ffcc33" >Movement</font>' {
				continue
			}
			'PAL Activity:' {
				state = ParsingState.pal
				continue
			}
			'NTSC Activity:' {
				state = ParsingState.ntsc
				continue
			}
			else {
				// Stay in same parsing mode
			}
		}

		// Parse data
		match state {
			.new_members {
				rankings << parse_new_member(date, line) or { continue }
			}
			.pal {
				rankings << parse_rankings(date, line, GameType.pal)
			}
			.ntsc {
				rankings << parse_rankings(date, line, GameType.ntsc)
			}
			else {}
		}
	}
	return rankings
}

fn parse_new_member(ranking_date time.Time, src string) ?Ranking {
	player, rank, game_type := parse_new_member_regex(src) or { return none }
	return Ranking{ranking_date, game_type, player, rank, true}
}

fn parse_rankings(ranking_date time.Time, src string, game_type GameType) []Ranking {
	mut results := []Ranking{}
	for player, rank in parse_rankings_regex(src) or { return [] } {
		results << Ranking{ranking_date, game_type, player, rank, false}
	}
	return results
}
