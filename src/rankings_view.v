module main

import vweb

['/rankings/:player/:game_mode'; get]
pub fn (mut app App) rankings_player(player string, game_mode string) !vweb.Result {
	mode := check_game_mode(game_mode) or {
		app.set_status(400, '')
		return app.text(err.str())
	}
	mode_str := match game_mode {
		'all' {
			'overall'
		}
		else {
			game_mode.to_upper()
		}
	}
	players := player.split(',')

	mut players_names := []string{}
	for player_id in players {
		player_id_int := player_id.parse_int(10, 64) or {
			players_names << player_id
			continue
		}
		db_player := app.service_get_player_by_id(int(player_id_int)) or { continue }
		players_names << db_player.name
	}

	title := '${players_names.join(', ')} ${mode_str} Leaderboard progress'
	rankings := app.service_get_rankings(players, mode) or { [] }
	app_version := app.version
	return $vweb.html()
}

['/rankings/:player'; get]
pub fn (mut app App) rankings_player_all(player string) !vweb.Result {
	return app.rankings_player(player, 'all')
}
