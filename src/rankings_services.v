module main

import database { Ranking }

fn (mut app App) service_get_rankings(players []string, game_type int) ![]Ranking {
	mut db := database.create_db_connection(app.config.db) or {
		println(err)
		return err
	}

	mut players_names := []string{}
	for player_id in players {
		player_id_int := player_id.parse_int(10, 64) or {
			players_names << player_id
			continue
		}
		db_player := app.service_get_player_by_id(int(player_id_int)) or { continue }
		players_names << db_player.name
	}

	mut results := []Ranking{}
	for player in players_names {
		if game_type == -1 {
			results << sql db {
				select from Ranking where player == player order by ranking_date
			}!
		} else {
			results << sql db {
				select from Ranking where player == player && game_type == game_type order by ranking_date
			}!
		}
	}

	results.sort(b.ranking_date > a.ranking_date)
	return results
}

pub fn check_game_mode(game_mode string) !int {
	return match game_mode.to_lower() {
		'all' {
			-1
		}
		'pal' {
			0
		}
		'ntsc' {
			1
		}
		else {
			error('Unknown game mode: ${game_mode}')
		}
	}
}
