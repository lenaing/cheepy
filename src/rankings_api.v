module main

import vweb

['/api/v1/rankings/:player/:game_mode'; get]
pub fn (mut app App) api_rankings_player(player string, game_mode string) !vweb.Result {
	mode := check_game_mode(game_mode) or {
		app.set_status(400, '')
		return app.json({
			'error': err.str()
		})
	}

	rankings := app.service_get_rankings([player], mode) or { [] }
	return app.json(rankings)
}
