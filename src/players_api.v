module main

import vweb

['/api/v1/player/:player'; get]
pub fn (mut app App) api_player(player_id string) !vweb.Result {
	player := if player_id.int() != 0 {
		app.service_get_player_by_id(player_id.int()) or {
			app.set_status(400, '')
			return app.json({
				'error': err.str()
			})
		}
	} else {
		app.service_get_player_by_name(player_id) or {
			app.set_status(400, '')
			return app.json({
				'error': err.str()
			})
		}
	}

	return app.json(player)
}
