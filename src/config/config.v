module config

import cli
import toml

const (
	default_listen_ip    = '127.0.0.1'
	default_listen_port  = 8080
	default_database_uri = 'sqlite:///smk.db'
)

pub struct DBConfig {
pub mut:
	driver   string
	username string
	password string
	host     string
	port     int
	database string
	query    string
}

pub struct Alias {
pub:
	player  string
	aliases []string
}

pub struct Config {
pub:
	ip           string
	port         int
	db           DBConfig
	do_not_track []string
	aliases      []Alias
}

pub fn parse_config(cmd cli.Command) !Config {
	mut listen_ip := ''
	mut listen_port := -1
	mut db_config := parse_database_uri(config.default_database_uri)
	mut do_not_track := []string{}
	mut aliases := []Alias{}

	// Load configuration file
	config_file := cmd.flags.get_string('config')!
	if config_file != '' {
		config_toml := toml.parse_file(config_file) or {
			panic("Failed to parse config file: '${config_file}'")
		}
		listen_ip = config_toml.value('listen').default_to(listen_ip).string()
		listen_port = config_toml.value('port').default_to(listen_port).int()

		database_uri := config_toml.value('database.uri').default_to(config.default_database_uri).string()

		db_config = parse_database_uri(database_uri)
		db_config.driver = config_toml.value('database.driver').default_to(db_config.driver).string()
		db_config.username = config_toml.value('database.username').default_to(db_config.username).string()
		db_config.password = config_toml.value('database.password').default_to(db_config.password).string()
		db_config.host = config_toml.value('database.host').default_to(db_config.host).string()
		db_config.port = config_toml.value('database.port').default_to(db_config.port).int()
		db_config.database = config_toml.value('database.dbname').default_to(db_config.database).string()
		db_config.query = config_toml.value('database.query').default_to(db_config.query).string()

		do_not_track << parse_toml_do_not_track(config_toml)
		aliases << parse_toml_aliases(config_toml)
	}

	// Load command line parameters
	cli_listen_ip := cmd.flags.get_string('listen')!
	if !cli_listen_ip.is_blank() {
		listen_ip = cli_listen_ip
	}
	cli_listen_port := cmd.flags.get_int('port')!
	if cli_listen_port > -1 {
		if cli_listen_port == 0 {
			eprintln('Failed to parse listen port from command line')
			exit(1)
		}
		listen_port = cli_listen_port
	}

	cli_database_uri := cmd.flags.get_string('database')!
	if !cli_database_uri.is_blank() {
		db_config = parse_database_uri(cli_database_uri)
	}

	// Ensure defaults
	if listen_ip == '' {
		listen_ip = config.default_listen_ip
	}
	if listen_port == -1 {
		listen_port = config.default_listen_port
	}

	return Config{
		ip: listen_ip
		port: listen_port
		db: db_config
		do_not_track: do_not_track
		aliases: aliases
	}
}

pub fn parse_database_uri(uri string) DBConfig {
	// db:driver:[//[user[:password]@][host][:port]/][dbname][?params][#fragment]

	uri_data := uri.split('/')
	driver := uri_data[0].trim_string_left('db:').trim_string_right(':')

	mut user := ''
	mut password := ''
	mut host := ''
	mut port := -1

	mut connect_data := uri_data[2].split('@')
	if connect_data.len > 1 {
		user_pass_data := connect_data[0].split(':')
		user = user_pass_data[0]
		if user_pass_data.len > 1 {
			password = user_pass_data[1]
		}
		connect_data.drop(1)
	}

	host_port_data := connect_data[0].split(':')
	host = host_port_data[0]
	if host_port_data.len > 1 {
		port = host_port_data[1].int()
	}

	database_data := uri_data#[3..].join('/').split('?')
	database := database_data[0]

	query := if database_data.len > 1 {
		database_data[1]
	} else {
		''
	}

	return DBConfig{driver, user, password, host, port, database, query}
}

fn parse_toml_do_not_track(doc toml.Doc) []string {
	doc_do_not_track := doc.value_opt('do_not_track') or { return [] }
	return (doc_do_not_track as []toml.Any).as_strings()
}

fn parse_toml_aliases(doc toml.Doc) []Alias {
	mut aliases := []Alias{}

	doc_aliases := doc.value_opt('aliases') or { return aliases }

	for alias in doc_aliases.array() {
		player := alias.value('player').string()
		player_aliases := (alias.value('aliases') as []toml.Any).as_strings()

		aliases << Alias{
			player: player
			aliases: player_aliases
		}
	}
	return aliases
}
