module main

import vweb

pub fn (mut app App) bagarre() vweb.Result {
	title := 'SMK Bagarre Comparator'

	mut countries_map := map[string]string{}
	for country in app.service_get_all_countries() or { [] } {
		countries_map[country.id] = country.name
	}

	players := app.service_get_all_players() or { [] }
	app_version := app.version
	return $vweb.html()
}
