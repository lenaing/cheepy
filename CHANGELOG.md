# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.1.0-alpha.3] - 2024-03-20

I'm on holiday, got some time for a spring release! 🚙💨🌺

### Added

- The BAGARRE generator is now available
- You can now request ranking through user name or ID
- Added country flags
- Added search filter on players list

### Fixed

- Switch to new parsing technique enabling to parse early news

## [0.1.0-alpha.2] - 2023-11-05

This release was made while watching awesome people on Twitch for the [Et Ta Cause](https://ettacause.fr/) event 🚙💨💙💜.

### Added

- Now you can request both PAL and NTSC mode at the same time
- The BAGARRE mode is on! You can now request multiple players' times
- Filter capabilities on displayed results:
  - Do not display results above and/or below a given rank
  - Do not display results before a given date
- You can switch to NTSC / PAL / both game type from results page

## [0.1.0-alpha.1] - 2023-10-21

Initial release
