# Cheepy

_cheepy_ is a tiny [Super Mario Kart Leaderboard](https://www.mariokart64.com/smk/) progress
tracker written in [V](https://vlang.io/) using the [vweb framework](https://modules.vlang.io/vweb.html).

## Features

* Rankings:
  * User info: ✓
  * User rankings in PAL / NTSC: ✓
* Bagarre:
  * Compare rankings between opponents: ✓

## Building

### Requirements

#### Database

You will need to install the _SQLite_ dependency.


| Database                                  | Dependency                                           |
|-------------------------------------------|------------------------------------------------------|
| [SQLite](https://www.sqlite.org/)         | [db.sqlite](https://modules.vlang.io/db.sqlite.html) |


### Building cheepy

```shell
v .
```

## Update the database

_cheepy_ use a local _SQLite_ database by default named `smk.db`.

Cheepy's database needs first to be created or updated by scrapping data from the leaderboard
website.

You can launch the updater by passing the `update` flag to _cheepy_. It will only scrape 
required data that haven't been scrapped yet from the leaderboard website and will update the
database with the new data found.

You can also force a full refresh by using the `force-update` flag in combination with the `update`
one.

```shell
./cheepy -update
🐟 Cheepy Updater
0893/0893 🚙 💨💨💨💨💨💨💨💨💨💨💨💨💨
```

## Running

### Defaults

Defaults to listening on `127.0.0.1:8080` with a local _SQLite_ database `smk.db`.

```shell
./cheepy
cheepy vX.Y.Z starting on 127.0.0.1:8080 ...
```

### Config file

You can specify a TOML configuration file:

```toml
listen = "0.0.0.0"
port = 8080

[database]
uri = "sqlite:///smk.db"
```

```shell
./cheepy -c config.toml
cheepy vX.Y.Z starting on 0.0.0.0:8080 ...
```

### Command line parameters

Command line parameters overrides defaults or config file settings.

```shell
./cheepy -c config.toml -l 192.168.0.1 -p 7777
cheepy vX.Y.Z starting on 192.168.0.1:7777 ...
```

### Default Database URI

_cheepy_ defaults to a local _SQLite_ database URI of `sqlite:///smk.db`.
