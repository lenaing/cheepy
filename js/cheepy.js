let players_data = [];
let players = [];
let max_rank = 1;

function clearFilter() {
    document.getElementById("notBefore").value = '';
    document.getElementById("notUnder").value = '';
    document.getElementById("notHigher").value = '';
}

function switchTo(game_mode) {
    const parsedUrl = new URL(window.location.href);
    let path_data = parsedUrl.pathname.split('/');
    path_data.pop();
    window.location.href=`${path_data.join("/")}/${game_mode}${document.location.search}`
}

async function load_rankings(players_ids, game_mode) {
    let params = new URLSearchParams(document.location.search);

    for (player_id of players_ids.replace("&#39;", "'").split(',')) {
        players.push(await d3.json(`/api/v1/player/${player_id}`));
    }
    players.sort(function (a, b) {
        return ('' + a.name).localeCompare(b.name);
    });

    for (const player of players) {
        if (game_mode == 'all') {
            players_data.push(await d3.json(`/api/v1/rankings/${player.id}/NTSC`));
            players_data.push(await d3.json(`/api/v1/rankings/${player.id}/PAL`));
        } else {
            players_data.push(await d3.json(`/api/v1/rankings/${player.id}/${game_mode}`));
        }
    }

    if (params.get('notBefore')) {
        let filterBefore = new Date(params.get('notBefore')).getTime() / 1000;
        players_data.forEach((player_data, index) => {
            players_data[index] = player_data.filter(entry => entry.ranking_date > filterBefore);
        });
    }

    if (params.get('notUnder')) {
        let filterNotUnder = params.get('notUnder');
        players_data.forEach((player_data, index) => {
            players_data[index] = player_data.filter(entry => entry.rank <= filterNotUnder);
        });
    }
    if (params.get('notHigher')) {
        let filterNotHigher = params.get('notHigher');
        players_data.forEach((player_data, index) => {
            players_data[index] = player_data.filter(entry => entry.rank >= filterNotHigher);
        });
        max_rank = filterNotHigher;
    }

    if (game_mode == 'all') {
        players = players.flatMap(player => {
            let player_ntsc = {...player}
            player_ntsc.name += ' NTSC'
            let player_pal = {...player}
            player_pal.name += ' PAL'
            return [player_ntsc, player_pal]
        })
    }

    render_rankings();
}

function render_rankings() {
    // Declare the chart dimensions and margins.
    const marginTop = 20;
    const marginRight = 20;
    const marginBottom = 30;
    const marginLeft = 40;

    // Set width and height depending on current window size.
    const width = window.innerWidth * 80 / 100;
    const height = window.innerHeight * 50 / 100;

    // Declare the x (horizontal position) scale.
    const x = d3.scaleUtc(d3.extent(players_data.flat(), d => d.ranking_date * 1000), [marginLeft, width - marginRight]);

    // Declare the y (vertical position) scale.
    const y = d3.scaleLinear([d3.max(players_data.flat(), d => d.rank), max_rank], [height - marginBottom, marginTop]);

    // Declare the line generator.
    const line = d3.line()
        .x(d => x(d.ranking_date * 1000))
        .y(d => y(d.rank));

    // Create the SVG container.
    const svg = d3.create("svg")
        .attr("style", "outline: thin solid black;")
        .attr("width", width)
        .attr("height", height+players.length*25);

    // Set background color
    svg.append("rect")
        .attr("width", "100%")
        .attr("height", "100%")
        .attr("fill", "white");

    // Add the x-axis.
    svg.append("g")
        .attr("transform", `translate(0,${height - marginBottom})`)
        .call(d3.axisBottom(x));

    // Add the y-axis.
    svg.append("g")
        .attr("transform", `translate(${marginLeft},0)`)
        .call(d3.axisLeft(y));

    const color = d3.scaleOrdinal()
                        .domain(players)
                        .range(d3.schemeCategory10);

    players.forEach(function (player, idx) {
        // Append a path for the player data.
        svg.append("path")
            .attr("fill", "none")
            .attr("stroke", color(idx))
            .attr("stroke-width", 2)
            .attr("d", line(players_data[idx]));

        // Append the legend dot.
        svg.append("circle")
            .attr("cx", 10)
            .attr("cy", height+10 + idx*25)
            .attr("r", 7)
            .style("fill", color(idx));

        // Append the legend text.
        svg.append('text')
            .attr('x', 40)
            .attr('y', height+15 + idx*25)
            .text(player.name);

        svg.append("svg:image")
            .attr('x', 20)
            .attr('y', height+4 + idx*25)
            .attr("xlink:href", `/assets/images/flags/${player.country}.png`)
    });

    // Append or replace the SVG element
    const old_node = container.children[0];
    if (old_node === undefined) {
        container.append(svg.node());
    } else {
        container.replaceChild(svg.node(), old_node);
    }
}


function updateBagarre(source, destination) {
    let sourceList = document.getElementById(source).options;
    let destinationList = document.getElementById(destination);
    let destinationOptions = destinationList.options;
    var l = sourceList.length;
    while (l--) {
        if (sourceList[l].selected) {
            destinationList.appendChild(sourceList[l]);
        }
    }

    let destinationOptionsSorted = [];
    for (var i = 0; i < destinationOptions.length; i++) {
        destinationOptionsSorted.push(destinationOptions[i]);
    }

    destinationOptionsSorted = destinationOptionsSorted.sort(function(a, b) {
        let aText = a.textContent;
        let bText = b.textContent;
        if (aText.toUpperCase() > bText.toUpperCase()) return 1;
        else if (aText.toUpperCase() < bText.toUpperCase()) return -1;
        else return 0;
    });

    for (var i = 0; i < destinationOptionsSorted.length; i++) {
        destinationOptions[i] = destinationOptionsSorted[i];
    }

}

function goBagarre() {
    let bagarreList = document.getElementById('readyToBagarre').options;
    let ids = [];

    for (var i = 0; i < bagarreList.length; i++) {
        if (bagarreList[i].selected) {
            ids.push(bagarreList[i].value);
        }
    }

    if (ids.length > 0) {
        window.location.href=`/rankings/${ids.join(',')}/`;
    }
}
