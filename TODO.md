# TODO

* Rankings
  * Scrapping:
    * Parse lines where numerous players updates are specified at the same time:
      `'#X <font color="00ffff" >Player X</font> and #Y <font color="00ffff" >Player Y</font> blah blah...'`
  * Live progress:
    * Have a module enable one to estimate her leaderboard progress without waiting for the
      weekly update
